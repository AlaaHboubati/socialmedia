const {validationResult} = require('express-validator');
const Post = require('../models/post');
const path = require('path');
const fs = require('fs');
const User = require('../models/user');
const post = require('../models/post');
const io = require('../socket');
const POSTS_PER_PAGE = 2;
exports.getPosts = (req,res,next)=>{
    const page = req.query.page || 1;
    let totalItems;
    Post.countDocuments()
    .then(numOfPosts=>{
        totalItems = numOfPosts;
        return Post.find()
                    .populate('creator')
                    .sort({createdAt:-1})
                    .skip((page-1)*POSTS_PER_PAGE)
                    .limit(POSTS_PER_PAGE);
                
    }).then(posts=>{
            res.status(200).json({
                message:'Fetched posts sucessfully!',
                posts:posts,
                totalItems:totalItems
            });
    })
    .catch(err=>{
            if(!err.statusCode){
                err.statusCode = 500;
            }
            next(err);
        })
        
}

exports.getPost = (req,res,next)=>{
    const postId = req.params.postId;
    Post.findById(postId)
    .then(post=>{
        if(!post){
            const error = new Error('Could not find post.');
            error.statusCode = 404;
            throw error;
        }
        res.status(200).json({
            message:'Post Fetched.',
            post:post
        });
    }).catch(err=>{
        if(!err.statusCode){
            err.statusCode = 500;
        }
        next(err);
    })
}

exports.createPost = (req,res,next)=>{
    const title = req.body.title;
    const content = req.body.content;
    const image = req.file;
    let creator;
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        const error = new Error('Validation failed,entered data is not correct.');
        error.statusCode = 422;
        throw error;
    }
    if(!image){
        const error = new Error('No image provided.');
        error.statusCode = 422;
        throw error;
    }
    const imageUrl = image.path;
    const post = new Post({
        title:title,
        content:content,
        imageUrl:imageUrl,
        creator:req.userId
    })
    post.save()
    .then(result=>{
       return User.findById(req.userId)
    }).then(user=>{
        creator = user;
        user.posts.push(post);
        return user.save()
    }).then(user=>{ 
        io.getIO().emit('posts',{action:'create',post:{...post._doc,creator:{_id:req.userId,name:creator.name}}});
        res.status(201).json({
            message:"post created suce ssfully!",
            post:post,
            creator:{_id:creator._id,name:creator.name}
        });
    }).catch(err=>{
        if(!err.statusCode){
            err.statusCode = 500;
        }
        next(err);
    })
 
}

exports.updatePost = (req,res,next)=>{
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        const error = new Error('Validation failed,entered data is not correct.');
        error.statusCode = 422;
        throw error;
    }
    const postId = req.params.postId;
    const title = req.body.title;
    const content = req.body.content;
    let imageUrl = req.body.image;
    if(req.file){
        imageUrl = req.file.path;
    }
   
    if(!imageUrl){
        const error = new Error('No file picked.');
        error.statusCode = 422;
        throw error;
    }
    Post.findById(postId)
    .populate('creator')
    .then(post=>{
        if(!post){
            const error = new Error('Could not find post.');
            error.statusCode = 404;
            throw error;
        }
        if(post.creator._id.toString() !== req.userId){
            const error = new Error('Not authorized.');
            error.statusCode = 403;
            throw error;
        }
        if(imageUrl !== post.imageUrl){
            clearImage(post.imageUrl);
        }
        post.title = title;
        post.content = content;
        post.imageUrl = imageUrl;
        return post.save();
    }).then(post=>{
        io.getIO().emit('posts',{action:'update',post:post});
        res.status(200).json({
            message:'Post updated.',
            post:post
        });
    })
    .catch(err=>{
        if(!err.statusCode){
            err.statusCode = 500;
        }
        next(err);
    })

    

}

exports.deletePost = (req,res,next)=>{
    const postId = req.params.postId;
    Post.findById(postId)
    .then(post=>{
        if(!post){
            const error = new Error('Could not find post.');
            error.statusCode = 404;
            throw error;
        }
        if(post.creator.toString() !== req.userId){
            const error = new Error('Not authorized.');
            error.statusCode = 403;
            throw error;
        }        
        clearImage(post.imageUrl);
        return Post.findByIdAndRemove(postId)
    })
    .then(result=>{
        return User.findById(req.userId);
    }).then(user=>{
        user.posts.pull(postId);
        return user.save();
    }).then(result=>{
        io.getIO().emit('posts',{action:'delete',post:postId});
        res.status(200).json({
            message:"Post has been deleted"
        });
    }).catch(err=>{
        if(!err.statusCode){
            err.statusCode = 500;
        }
        next(err);
    })
}

const clearImage = filePath =>{
    filePath = path.join(__dirname,'..',filePath);
    fs.unlink(filePath,(err)=>{console.log(err)});
}
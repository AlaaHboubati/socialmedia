const express = require('express');
const router = express.Router();
const feedController = require('../Controllers/feed');
const {body} = require('express-validator');
const isAuth = require('../middleware/is-auth');
//GET /posts
router.get('/posts',isAuth,feedController.getPosts);


//POST /post
router.post('/post',isAuth,[
    body('title')
    .trim()
    .isLength({min:5}),
    body('content')
    .trim()
    .isLength({min:5})
],feedController.createPost);


router.get('/post/:postId',isAuth,feedController.getPost)
router.put('/post/:postId',isAuth,[
    body('title')
    .trim()
    .isLength({min:5}),
    body('content')
    .trim()
    .isLength({min:5})
],feedController.updatePost);

router.delete('/post/:postId',isAuth,feedController.deletePost);

module.exports = router;


const express = require('express');
const router = express.Router();
const {body} = require('express-validator');
const User = require('../models/user');
const authController = require('../Controllers/auth');

router.put('/signup',[
    body('email')
    .isEmail()
    .withMessage('Please enter a valid email.')
    .custom((value,{req})=>{
        return User.findOne({email:req.email})
        .then(user=>{
            if(user){
                return Promise.reject('E-mail already exists,please try a different one.');
            }
        })
    }).normalizeEmail(),
    body('password')
    .trim()
    .isLength({min:5}),
    body('name')
    .trim()
    .not()
    .isEmpty()
],authController.signup);

router.post('/login',authController.login);

module.exports = router;